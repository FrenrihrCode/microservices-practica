const { DataTypes } = require("sequelize/types");
const { sequelize } = require("../setting");

const Model = sequelize.define("curso", {
  name: {
    type: DataTypes.STRING,
  },
  edad: {
    type: DataTypes.BIGINT,
  },
  color: {
    type: DataTypes.STRING,
  },
});

const SyncDB = () => {
  try {
    await Model.sync();
  } catch (error) {
    console.log(error);
  }
};

module.exports = { SyncDB, Model };
