const {
  queueView,
  queueCreate,
  queueUpdate,
  queueDelete,
  queueFindOne,
} = require(".");
const Services = require("../Services");
const { internalError } = require("../setting");

queueView.process(async (job, done) => {
  try {
    const {} = job.data;
    const { statusCode, message, data } = await Services.View({});

    done(null, { statusCode, message, data });
  } catch (error) {
    console.log({ step: "adapter queueView", error: error.toString() });
    done(null, { statusCode: 500, message: internalError });
  }
});

queueCreate.process(async (job, done) => {
  try {
    const { age, color, name } = job.data;
    const { statusCode, message, data } = await Services.Create({
      age,
      color,
      name,
    });

    done(null, { statusCode, message, data });
  } catch (error) {
    console.log({ step: "adapter queueCreate", error: error.toString() });
    done(null, { statusCode: 500, message: internalError });
  }
});

queueDelete.process(async (job, done) => {
  try {
    const { id } = job.data;
    const { statusCode, message, data } = await Services.Delete({ id });

    done(null, { statusCode, message, data });
  } catch (error) {
    console.log({ step: "adapter queueDelete", error: error.toString() });
    done(null, { statusCode: 500, message: internalError });
  }
});

queueUpdate.process(async (job, done) => {
  try {
    const { id, name, age, color } = job.data;
    const { statusCode, message, data } = await Services.Update({
      id,
      name,
      age,
      color,
    });

    done(null, { statusCode, message, data });
  } catch (error) {
    console.log({ step: "adapter queueUpdate", error: error.toString() });
    done(null, { statusCode: 500, message: internalError });
  }
});

queueFindOne.process(async (job, done) => {
  try {
    const { name } = job.data;
    const { statusCode, message, data } = await Services.FindOne({ name });

    done(null, { statusCode, message, data });
  } catch (error) {
    console.log({ step: "adapter queueFindOne", error: error.toString() });
    done(null, { statusCode: 500, message: internalError });
  }
});
