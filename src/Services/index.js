const Controllers = require("../Controllers");

async function Create({ age, name, color }) {
  try {
    const { statusCode, data, message } = await Controllers.Create({
      age,
      name,
      color,
    });

    return { statusCode, data, message };
  } catch (error) {
    console.log({ step: "service Create", error: error.toString() });
    return { statusCode: 500, data: null, message: error.toString() };
  }
}

async function Delete({ id }) {
  try {
    const { statusCode, data, message } = await Controllers.Delete({
      where: { id },
    });

    return { statusCode, data, message };
  } catch (error) {
    console.log({ step: "service Delete", error: error.toString() });
    return { statusCode: 500, data: null, message: error.toString() };
  }
}

async function Update({ age, name, color, id }) {
  try {
    const { statusCode, data, message } = await Controllers.Update({
      age,
      name,
      color,
      id,
    });

    return { statusCode, data, message };
  } catch (error) {
    console.log({ step: "service Update", error: error.toString() });
    return { statusCode: 500, data: null, message: error.toString() };
  }
}

async function FindOne({ name }) {
  try {
    const { statusCode, data, message } = await Controllers.FindOne({
      where: { name },
    });

    return { statusCode, data, message };
  } catch (error) {
    console.log({ step: "service FindOne", error: error.toString() });
    return { statusCode: 500, data: null, message: error.toString() };
  }
}

async function View({}) {
  try {
    const { statusCode, data, message } = await Controllers.View({});

    return { statusCode, data, message };
  } catch (error) {
    console.log({ step: "service View", error: error.toString() });
    return { statusCode: 500, data: null, message: error.toString() };
  }
}

module.exports = { Create, Update, Delete, FindOne, View };
