const {
  queueCreate,
  queueDelete,
  queueFindOne,
  queueUpdate,
  queueView,
} = require("../src/Adapters");

async function Create({ age, color, name }) {
  try {
    const job = queueCreate.add({ age, color, name });
    const result = await job.finished();
    console.log(result);
  } catch (error) {
    console.log(error);
  }
}

async function Delete({ id }) {
  try {
    const job = queueDelete.add({ id });
    const result = await job.finished();
    console.log(result);
  } catch (error) {
    console.log(error);
  }
}

async function FindOne({ name }) {
  try {
    const job = queueFindOne.add({ name });
    const result = await job.finished();
    console.log(result);
  } catch (error) {
    console.log(error);
  }
}

async function Update({ age, color, name, id }) {
  try {
    const job = queueUpdate.add({ age, color, name, id });
    const result = await job.finished();
    console.log(result);
  } catch (error) {
    console.log(error);
  }
}

async function View({}) {
  try {
    const job = queueView.add({});
    const result = await job.finished();
    console.log(result);
  } catch (error) {
    console.log(error);
  }
}

async function main() {
  View();
}

main();
